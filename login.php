<?php
include('loginv.php'); // Includes Login Script

if(isset($_SESSION['login_user'])){
header("location:welcomeasset.php");
 }
 ?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
    width: 20%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}


.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: left;
    padding-top: 1px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    
}
</style>
</head>
<body>

<h1 align=center col> <font color="Blue">Login Form</font></h1>

<form action="" method=post>
  <div class="imgcontainer">
    <img src="laptop.jpeg" alt="Avatar" class="avatar">
  </div>

  <div class="container">
   <label for="username"><b>username</b></label>
    <input type="text" placeholder="Enter Username" name="username" >

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" >
        
    <input type="submit" name=submit value=login style="font-size:15pt;color:white;background-color:green;border:2px solid #336600;padding:3px"></input>
    <span><?php echo $error; ?></span>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
  </div>

  
  
    <span class="psw">Forgot <a href="#">password?</a></span>
  </div>
</form>


</body>
</html>